---
layout: page
title: About
permalink: /about/
---
I'm Eli, and this is my online notepad.
Vim is great, but scp is hard sometimes.

You'll see a ton of work in progress, and notes on things I'm currently trying to fix/learn about.






---Potentially Important Info About Jekyll---

This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
